import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent {

  nombreLower: string = 'Daniel';
  nombreUpper: string = 'DANIEL';
  nombreCompleto: string = 'dAnIeL rEStRePo';

  fecha: Date = new Date();


}
