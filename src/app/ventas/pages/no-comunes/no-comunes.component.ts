import { Component } from '@angular/core';

import { interval } from 'rxjs';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})
export class NoComunesComponent {
  //i18nSelect
  nombre: string = 'Daniel';
  genero: string = 'masculino';

  invitacionMapa = {
    'masculino': 'invitarlo',
    'femenino' : 'invitarla'
  }

  // i18nPlural
  clientes: string[] = ['Maria','Juan','Julian','felipe','joseS'];
  clientesMapa = {
    '=0': 'no tenemos ningun cliente esperando',
    '=1': 'tenemos un cliente esperando',
    'other': 'tenemos # clientes esperando'
  }

  cambiarPersona(){
    this.nombre = 'Melissa';
    this.genero = 'femenino'
  }

  borrarCliente(){
    this.clientes.pop();
  }

  //KeyValue Pipe
  persona = {
    nombre: 'Daniel',
    edad: 35,
    dirección: 'Otawa, Canadá'
  }

  //JsonPipe
  heroes = [
    {
      nombre: 'Superman',
      vuela:true
    },
    {
      nombre: 'Robin',
      vuela: false
    },
    {
      nombre: 'Batman',
      vuela: false
    }
  ]

  //Async Pipe
  miObservable = interval(1000);
  resolveP = false;
  valorPromesa = new Promise( (resolve, reject) => {
    setTimeout(() => {
      this.resolveP = true
      resolve('Tenemos data de promesa')
    }, 3500);
    
  })
}
