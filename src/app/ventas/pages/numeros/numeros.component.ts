import { Component } from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styles: [
  ]
})
export class NumerosComponent {

  ventasNetas: number = 223344.55544;
  porcentaje : number = 0.4557;

}
